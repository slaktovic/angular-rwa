import { ActionReducerMap } from '@ngrx/store';
import { trainingReducer } from '../training/training.reducer';
import { votingReducer } from '../training/voting/voting.reducer';

export const reducers: ActionReducerMap<any> = {
    training: trainingReducer,
    voting: votingReducer
}

