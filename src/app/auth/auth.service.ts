import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore } from 'angularfire2/firestore';

import { User } from './models/user.model';
import { AuthData } from './models/auth-data.model';
import { UIService } from '../shared/ui.service';


@Injectable()
export class AuthService {
    authChange = new Subject<boolean>();
    private isAuthenticated = false;

    constructor(
        private router: Router,
        private afAuth: AngularFireAuth,
        private afs: AngularFirestore,
        private uiService: UIService
    ){}

    registerUser(user: User) {
        
        let createUser = this.afAuth.auth.createUserWithEmailAndPassword(user.email, user.password);
        let addToCollection = this.afs.collection('users').ref.doc(user.email).set(user);
        
        let promiseArr = [createUser,addToCollection];
    
        Promise.all([createUser,addToCollection])
        .then( res => {
            localStorage.setItem('email',user.email);
            this.isAuthenticated = true;
            this.uiService.showSnackbar("You've successfully registered!");
        })
        .catch( err => this.uiService.showSnackbar(err));
    }
    

    login(authData: AuthData) {
        this.afAuth.auth.signInWithEmailAndPassword(authData.email, authData.password)
        .then(res => {
            localStorage.setItem('email',authData.email);
            this.isAuthenticated = true;
            this.uiService.showSnackbar("You've successfully logged in!");
        })
        .catch( err => this.uiService.showSnackbar(err));
    }

    logout() {
        localStorage.removeItem('email');
        this.afAuth.auth.signOut();
    }

    isAuth() {
        return this.isAuthenticated;
    }

    initAuthListener() {
        this.afAuth.authState.subscribe( user => {
            if(user) {
                this.isAuthenticated = true;
                this.authChange.next(true);
                this.router.navigate(['/']);
            }else {
                this.isAuthenticated = false;
                this.authChange.next(false);
                this.router.navigate(['login']);
            }
        });
    }
}