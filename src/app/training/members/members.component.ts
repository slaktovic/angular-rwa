import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { User } from '../../auth/models/user.model';
import { Subscription } from 'rxjs';
import { UIService } from '../../shared/ui.service';

@Component({
  selector: 'app-members',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.css']
})
export class MembersComponent implements OnInit, OnDestroy {

  private userCollection: AngularFirestoreCollection<User>;
  private userSubs: Subscription[] = [];
  private members: Array<User>;
  private voteCount: Number;

  constructor(
    @Inject(MAT_DIALOG_DATA) private passedData: any,
    private afs: AngularFirestore,
    private uiService: UIService
  ) { }
 
  ngOnInit() {
    this.userCollection = this.afs.collection('trainings')
      .doc(this.passedData.trainingId)
      .collection<User>('members');
    
    let sub = this.userCollection.snapshotChanges()
      .map( docs => docs.map( doc => {
                          return {
                            name: doc.payload.doc.data().name,
                            lastName: doc.payload.doc.data().lastName,
                            email: doc.payload.doc.data().email,
                            password: doc.payload.doc.data().password
                          } as User; 
                        })).subscribe((members)=> { 
                          this.members = members}, 
                          error => this.uiService.showSnackbar(error));
    this.userSubs.push(sub);

    let countSub = this.afs.collection('trainings')
      .doc(this.passedData.trainingId)
      .collection('votes').snapshotChanges()
      .map( docs => docs.map( doc => doc.payload.doc.id))
      .subscribe((ids)=> { 
        this.voteCount = ids.reduce((acc,id) => acc = acc + 1,0);                          
      }, 
      error => this.uiService.showSnackbar(error));
    this.userSubs.push(countSub);
  }

  ngOnDestroy() {
    this.userSubs.forEach( sub => sub.unsubscribe());
  }
}
