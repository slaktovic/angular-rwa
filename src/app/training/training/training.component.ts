import { Component, OnInit, OnDestroy } from '@angular/core';

import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import * as actions from '../training.actions';
import * as fromTraining from '../training.reducer';
import * as fromVoting from '../voting/voting.reducer';
import * as votingActions from '../voting/voting.actions';


import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Subscription } from 'rxjs';
import { TrainingService } from '../training.service';
import { MatDialog } from '@angular/material';
import { MembersComponent } from '../members/members.component';
import { UIService } from '../../shared/ui.service';

@Component({
  selector: 'app-training',
  templateUrl: './training.component.html',
  styleUrls: ['./training.component.css']
})
export class TrainingComponent implements OnInit, OnDestroy {
  
  private trainingCollection: AngularFirestoreCollection<fromTraining.Training>;
  private trainingSubs: Subscription[] = [];
  private trainings: Observable<any>;

  constructor(
    private store: Store<fromTraining.State>,
    private votingStore: Store<fromVoting.State>,
    private afs: AngularFirestore,
    private trainingService: TrainingService,
    private dialog: MatDialog,
    private uiService: UIService ) { }

  ngOnInit() {
    this.trainingCollection = this.afs.collection<fromTraining.Training>('trainings');
    
    let sub = this.trainingCollection.snapshotChanges()
      .map( docs => docs.map( doc => {
                          return {
                            id: doc.payload.doc.id,
                            plan: doc.payload.doc.data().plan,
                            date: doc.payload.doc.data().date,
                          } as fromTraining.Training; 
                        })).subscribe((trainings)=> { 
                          this.store.dispatch(new actions.AddAll(trainings))}, 
                          error => this.uiService.showSnackbar(error));
    this.trainingSubs.push(sub);
    this.trainings = this.store.select(fromTraining.selectAll);
  }

  joinTraining(trainingId) {
    this.trainingService.join(trainingId);
    this.uiService.showSnackbar("You signed up for training!")
  }
 
  leaveTraining(trainingId) {
    this.trainingService.leave(trainingId);
    this.uiService.showSnackbar("You opted out of training!");
  }

  upvoteTraining(trainingId) {
    const payload = { trainingId }
    this.votingStore.dispatch(new votingActions.VoteUpdate(payload));
    this.uiService.showSnackbar("You upvoted the training!");
  }

  deleteTraining(id) {
    this.afs.collection('trainings').ref.doc(id).delete()
      .then( () => { 
        this.store.dispatch( new actions.Delete(id))
        this.uiService.showSnackbar("You deleted the training!");
      });
  }
  
  showMembers(trainingId) {
    this.dialog.open(MembersComponent, { data: { trainingId }});
  }

  ngOnDestroy() {
    this.trainingSubs.forEach( sub => sub.unsubscribe());
  }
}
 