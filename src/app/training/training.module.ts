import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TrainingComponent } from './training/training.component';
import { StoreModule } from '@ngrx/store';
import { trainingReducer } from './training.reducer';
import { MaterialModule } from '../material.module';
import { FormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { NewtrainingComponent } from './newtraining/newtraining.component';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    FlexLayoutModule,
    FormsModule,
    AngularFireDatabaseModule,
    StoreModule.forFeature('training', trainingReducer)
  ],
  declarations: [TrainingComponent, NewtrainingComponent]
})
export class TrainingModule { }
