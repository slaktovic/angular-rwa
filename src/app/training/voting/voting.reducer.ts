import * as VotingActions from './voting.actions';

export type Action = VotingActions.All;

export interface State {
    error: string;
    success: string;
}

const initialState: State = {
    error: null,
    success: null
};

export function votingReducer(state: State, action: Action) {

  switch (action.type) {
    case VotingActions.VOTE_UPDATE:
      return { ...state, error: null, success: null };

    case VotingActions.VOTE_SUCCESS:
      return { ...state, ...action.payload };

    case VotingActions.VOTE_FAIL:
      return { ...state, ...action.payload };

    default:
      return state;
  }
}