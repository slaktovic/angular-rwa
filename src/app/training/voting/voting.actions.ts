import { Action } from '@ngrx/store';

export const VOTE_UPDATE  = '[Training] Vote';
export const VOTE_SUCCESS = '[Training] Vote success';
export const VOTE_FAIL    = '[Training] Vote fail';

export class VoteUpdate implements Action {
    readonly type = VOTE_UPDATE;
    constructor(public payload: any) {}
}
  
export class VoteSuccess implements Action {
    readonly type = VOTE_SUCCESS;
    constructor(public payload?: any) {}
}
  
export class VoteFail implements Action {
    readonly type = VOTE_FAIL;
    constructor(public payload?: any) {}
}

export type All = VoteUpdate | VoteSuccess | VoteFail;