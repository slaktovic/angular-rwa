import { Injectable }                 from '@angular/core';
import { Effect, Actions }            from '@ngrx/effects';

import { Observable }                 from 'rxjs/Observable';
import { of }                         from 'rxjs/observable/of';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/catch';

import * as votingActions             from '../voting/voting.actions';
import { AngularFirestore }           from 'angularfire2/firestore';
export type Action = votingActions.All;


@Injectable()
export class VotingEffects {

  constructor(private actions: Actions, private afs: AngularFirestore) {}

  @Effect()
  voteUpdate: Observable<Action> = this.actions.ofType(votingActions.VOTE_UPDATE)
    .map((action: votingActions.VoteUpdate) => action.payload)
    .mergeMap(payload => of(this.afs.collection('trainings').doc(payload.trainingId)
                         .collection('votes').doc(new Date().toString()).set({payload: payload.trainingId}))
    .map(() => new votingActions.VoteSuccess({ success: "Voting succeeded"}))
    .catch(err => of (new votingActions.VoteFail( { error: "Voting failed." } ))));
}