import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';

@Injectable()
export class TrainingService {

  constructor(
    private afs: AngularFirestore
  ) {}


  join(trainingId) {
    const training = this.afs.collection('trainings').doc(trainingId);
    const user = this.afs.collection('users').doc(localStorage.getItem('email'));
  
    user.snapshotChanges()
      .forEach(doc => {
        training.collection('members').doc(localStorage.getItem('email')).set(doc.payload.data());
      });
  }
  
  leave(trainingId) {
    const training = this.afs.collection('trainings')
      .doc(trainingId)
      .collection('members')
      .doc(localStorage.getItem('email'))
      .delete();
  }
}