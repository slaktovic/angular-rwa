import { Action } from '@ngrx/store';
import { Training } from './training.reducer';

export const CREATE  = '[Trainings] Create';
export const DELETE  = '[Trainings] Delete';
export const ADD_ALL = '[Trainings] Add all';

export class Create implements Action {
    readonly type = CREATE;
    constructor(public training: Training ) {}
}
export class Delete implements Action {
    readonly type = DELETE;
    constructor(public id: string) {}
}

export class AddAll implements Action {
    readonly type = ADD_ALL;
    constructor(public trainings: Training[]) {}
}
 
export type TrainingActions = Create | Delete | AddAll;