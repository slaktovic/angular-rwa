import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

import { AngularFirestore } from 'angularfire2/firestore';
import { Store } from '@ngrx/store';
import * as actions from '../training.actions';
import * as fromTraining from '../training.reducer';
import { UIService } from '../../shared/ui.service';


@Component({
  selector: 'app-newtraining',
  templateUrl: './newtraining.component.html',
  styleUrls: ['./newtraining.component.css']
})
export class NewtrainingComponent implements OnInit {

  private isFormVisible = false;

  constructor(
    private afs: AngularFirestore,
    private store: Store<fromTraining.State>,
    private uiService: UIService) { }

  ngOnInit() {
  }

  toggleTrainingForm() {
    this.isFormVisible = !this.isFormVisible;
  }

  createTraining(form: NgForm) {

    let training = { plan: form.value.plan, date: form.value.date }
    
    this.afs.collection('trainings').add(training)
    .then(response => {
      if(!response.id) {
        return;
      }

      let training = {
        id: response.id,
        plan: form.value.plan,
        date: form.value.date
      };
      this.store.dispatch(new actions.Create(training));
      this.uiService.showSnackbar("You've created a training!");
    });      
  }
}
