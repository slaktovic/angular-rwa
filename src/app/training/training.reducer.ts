import * as actions from './training.actions';
import { EntityState, createEntityAdapter } from '@ngrx/entity';
import { createFeatureSelector } from '@ngrx/store';

export interface Training {
    id: string;
    plan: string;
    date: Date;
}

export const trainingAdapter = createEntityAdapter<Training>();
export interface State extends EntityState<Training> {}

const defaultTraining = {
    ids: [],
    entities: {}
}

export const initialState: State = trainingAdapter.getInitialState(defaultTraining);

export function trainingReducer(state: State = initialState, action: actions.TrainingActions) {
    switch(action.type) {
        case actions.CREATE:
            return trainingAdapter.addOne(action.training, state);
        case actions.DELETE:
            return trainingAdapter.removeOne(action.id, state);     
        case actions.ADD_ALL:
            return trainingAdapter.addAll(action.trainings,state);
        
        default:
            return state;
    }
}

export const getTrainingState = createFeatureSelector<State>('training');

export const {
    selectIds,
    selectEntities,
    selectAll,
    selectTotal,
} = trainingAdapter.getSelectors(getTrainingState);