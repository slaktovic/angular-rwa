import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { MatSnackBar } from '@angular/material';

@Injectable()
export class UIService {

    constructor(private snackbar: MatSnackBar) {}
    showSnackbar(message) {
        this.snackbar.open(message, null,{ duration: 2000 });
    }
}