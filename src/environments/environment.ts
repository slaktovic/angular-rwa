// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAS83vKW7JeWe1I9pzSFCclKyZISyeYf1o",
    authDomain: "rwa-angular-project.firebaseapp.com",
    databaseURL: "https://rwa-angular-project.firebaseio.com",
    projectId: "rwa-angular-project",
    storageBucket: "rwa-angular-project.appspot.com",
    messagingSenderId: "581068018352"
  }
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
